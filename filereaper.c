/* Copyright (c) 2015-2016 Stefan Tatschner
 *
 * filereaper.c
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <dirent.h>
#include <getopt.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <regex.h>
#include <time.h>
#include <unistd.h>
#include <libgen.h>
#include "iniparser.h"
#include "types.h"
#include "helpers.h"
#include "constants.h"
#include "version.h"

unsigned int currentdepth = 0;

static int reap(struct config* conf)
{
    DIR* dir;
    int ret = REAP_OK;
    char currentfile[PATH_MAX];
    time_t timestamp;
    time_t age, age_days;
    struct dirent* direntry;
    struct stat st;

    printf("Entering %s: %s\n", conf->section, conf->path);
    dir = opendir(conf->path);
    currentdepth++;

    if (dir == NULL) {
        perror("opendir(3)");
        ret = REAP_BADIO;
        goto cleanup;
    }

    /* TODO: http://rosettacode.org/wiki/Walk_a_directory/Recursively#Library:_POSIX */
    while ((direntry = readdir(dir)) != NULL) {
        /* Build absolute pathname of the current file */
        sprintf(currentfile, "%s/%s", conf->path, direntry->d_name);

        /* Skip special "." and ".." paths. */
        if ((strncmp(basename(currentfile), ".", 1) == 0) ||
             strncmp(basename(currentfile), "..", 2) == 0)
            continue;

        if (stat(currentfile, &st))
            perror(currentfile);

        /* Recurse into subdirectories when it is explicitely enabled. */
        if (S_ISDIR(st.st_mode)) {
            if (conf->flags & R_RECURSIVE) {
                if (currentdepth >= MAXDEPTH) {
                    fprintf(stderr, "Warning: MAXDEPTH %i reached\n", MAXDEPTH);
                    fprintf(stderr, "Skipping %s\n", conf->path);
                    continue;
                }
                /* We just need to change the path attribute.
                 * So let's just copy the whole struct and overwrite the
                 * path attribute with the apppropriate subdirectory. */
                struct config sub_conf;
                memcpy(&sub_conf, conf, sizeof(sub_conf));
                sub_conf.path = currentfile;

                printf("Recursing into subdirectory %s\n", currentfile);
                if (reap(&sub_conf) != REAP_OK) {
                    fprintf(stderr, "An error occured during reaping %s\n", sub_conf.section);
                    fprintf(stderr, "Skipping %s\n", sub_conf.path);
                    continue;
                }
            } else {
                printf("Skipping directory %s\n", currentfile);
                continue;
            }
        }

        timestamp = time(NULL);
        age = timestamp - st.st_mtim.tv_sec;
        age_days = age / 3600 / 24;

        if (age_days > conf->age) {
            if (conf->flags & R_FORCE) {
                printf("Reaping: %s\n", currentfile);
                if (unlink(currentfile) < 0) {
                    perror("unlink(2)");
                    ret = REAP_BADIO;
                    goto cleanup;
                }
            }
            else {
                printf("Would reap: %s\n", currentfile);
            }

            if (conf->flags & R_VERBOSE) {
                printf("Age: %lld day(s)\n", (long long int) age_days);
                printf("Reap after: %i day(s)\n", conf->age);
            }
        } else {
            if (conf->flags & R_VERBOSE) {
                printf("Would NOT reap: %s\n", currentfile);
                printf("Age: %lld day(s)\n", (long long int) age_days);
                printf("Reap after: %i day(s)\n", conf->age);
            }
        }
    }

    (void)closedir(dir);

cleanup:
    currentdepth--;

    return ret;
}

static void version_info(void)
{
    printf("%s %s\n", PROJECT_NAME, VERSION);
    printf("\n");
    printf("               ...\n"
           "             ;::::;\n"
           "           ;::::; :;\n"
           "         ;:::::'   :;\n"
           "        ;:::::;     ;.\n"
           "       ,:::::'       ;           OOO\\\n"
           "       ::::::;       ;          OOOOO\\\n"
           "       ;:::::;       ;         OOOOOOOO\n"
           "      ,;::::::;     ;'         / OOOOOOO\n"
           "    ;:::::::::`. ,,,;.        /  / DOOOOOO\n"
           "  .';:::::::::::::::::;,     /  /     DOOOO\n"
           " ,::::::;::::::;;;;::::;,   /  /        DOOO\n"
           ";`::::::`'::::::;;;::::: ,#/  /          DOOO\n"
           ":`:::::::`;::::::;;::: ;::#  /            DOOO\n"
           "::`:::::::`;:::::::: ;::::# /              DOO\n"
           "`:`:::::::`;:::::: ;::::::#/               DOO\n"
           " :::`:::::::`;; ;:::::::::##                OO\n"
           " ::::`:::::::`;::::::::;:::#                OO\n"
           " `:::::`::::::::::::;'`:;::#                O\n"
           "  `:::::`::::::::;' /  / `:#\n"
           "   ::::::`:::::;'  /  /   `#\n");
    printf("\n");
    printf("Built on: %s\n", BUILD_PLATFORM);
    printf("Built at: %s\n", BUILD_DATE);
    printf("Built by: %s\n", COMPILER);
    printf("Build System: cmake %s\n", CMAKE_VERSION);
}

static void usage_info(void)
{
    fprintf(stderr, "usage: %s [-c path] [-f] [-v] [-V] [-h]\n", PROJECT_NAME);
    fprintf(stderr, "\n");
    fprintf(stderr, "options:\n");
    fprintf(stderr, "  -c path   [c]onfig:  Use a custom config file\n");
    fprintf(stderr, "  -f        [f]orce:   Do not perform a dry run\n");
    fprintf(stderr, "  -v        [v]erbose: Output more (debug) information\n");
    fprintf(stderr, "  -V        [V]ersion: Show version information\n");
    fprintf(stderr, "  -h        [h]elp:    Show this page and exit\n");
}

int main(int argc, char** argv)
{
    int i;
    int flags;
    int nsections;
    int opt;
    int ret = EXIT_SUCCESS;
    char config_path[PATH_MAX];
    dictionary* conf;

    flags = R_NONE;
    config_path[0] = '\0';

    /* Parse command line arguments. */
    while ((opt = getopt(argc, argv, "c:fvVh")) != -1) {
        switch (opt) {
        case 'c':
            strncpy(config_path, optarg, PATH_MAX - 1);
            break;
        case 'f':
            flags |= R_FORCE;
            break;
        case 'v':
            flags |= R_VERBOSE;
            break;
        case 'V':
            version_info();
            return EXIT_SUCCESS;
        case 'h':
            usage_info();
            return EXIT_SUCCESS;
        default: /* '?' */
            usage_info();
            return EXIT_FAILURE;
        }
    }

    /* Filereaper starts here; first read the configfile. */
    printf("%s %s\n", PROJECT_NAME, VERSION);

    if (!(flags & R_FORCE))
        printf("*** Dry run; no files will be deleted! ***\n");

    if (strempty(config_path)) {
        if (get_config_path(config_path) < 0) {
            fprintf(stderr, "Could not determine config path!\n");
            fprintf(stderr, "Leaving...\n");
            return EXIT_FAILURE;
        }
    }

    if (flags & R_VERBOSE)
        printf("Using config file: %s\n", config_path);

    /* After this allocation we have to perform cleanups. */
    conf = iniparser_load(config_path);

    if (conf == NULL) {
        /* libiniparser prints a nice error message; "Leaving..." is enough. */
        fprintf(stderr, "Leaving...\n");
        ret = EXIT_FAILURE;
        goto cleanup;
    }

    nsections = iniparser_getnsec(conf);

    /* Interate through config sections and call reap() for each of them. */
    for (i = 0; i < nsections; i++) {
        struct config sect_conf;
        parse_config_section(conf, i, &sect_conf);

        /* Skip default section */
        if (!strncmp(sect_conf.section, "default", 7))
            continue;

        if (strempty(sect_conf.path)) {
            fprintf(stderr, "No path specified for %s. Skipping...\n", sect_conf.section);
            continue;
        }

        /* Add flags from the command line */
        sect_conf.flags |= flags;

        if (reap(&sect_conf) != REAP_OK) {
            fprintf(stderr, "An error occured during reaping %s\n", sect_conf.section);
            fprintf(stderr, "Skipping %s: %s\n", sect_conf.section, sect_conf.path);
            continue;
        }
    }

cleanup:
    iniparser_freedict(conf);
    printf("Finished.\n");

    return ret;
}
