# Copyright (c) 2015-2016 Stefan Tatschner
#
# doc/Makefile
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

ASCIIDOC = asciidoctor
# asciidoctor 1.5.3 provides a manpage backend, but that's not ready yet.
# to build the manpages with asciidoctor "make" has to be invoked like:
#   $ make ASCIIDOC_MAN=asciidoctor ASCIIDOC_MAN_FLAGS=" -d manpage -b manpage"
ASCIIDOC_MAN = a2x
ASCIIDOC_MAN_FLAGS  = -f manpage
ASCIIDOC_HTML_FLAGS =

MAN1_TXTS = $(wildcard *.1.txt)
MAN5_TXTS = $(wildcard *.5.txt)
DOC_MAN1  = $(patsubst %.txt,%,$(MAN1_TXTS))
DOC_MAN5  = $(patsubst %.txt,%,$(MAN5_TXTS))
MAN_TXT   = $(MAN1_TXTS) $(MAN5_TXTS)
DOC_HTML  = $(patsubst %.txt,%.html,$(MAN_TXT))
INCLUDES  = footer.txt
CSS       =

all: html man

html: $(DOC_HTML)

man: man1 man5
man1: $(DOC_MAN1)
man5: $(DOC_MAN5)

%.1 : %.1.txt $(INCLUDES)
	$(ASCIIDOC_MAN) $(ASCIIDOC_MAN_FLAGS) $<

%.5 : %.5.txt $(INCLUDES)
	$(ASCIIDOC_MAN) $(ASCIIDOC_MAN_FLAGS) $<

%.html : %.txt $(INCLUDES) $(CSS)
	$(ASCIIDOC) $(ASCIIDOC_HTML_FLAGS) $<
