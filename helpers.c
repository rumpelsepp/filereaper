/* Copyright (c) 2015-2016 Stefan Tatschner
 *
 * utils.c
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pwd.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <regex.h>
#include "iniparser.h"
#include "types.h"
#include "constants.h"

int strempty(const char *s)
{
    /* http://stackoverflow.com/a/7970669 */
    if ((s != NULL) && (s[0] == '\0'))
        return true;
    else
        return false;
}

int strendswith(const char* str, const char* suffix) {
    size_t str_len;
    size_t suffix_len;
    size_t offset;

    if(str == NULL || suffix == NULL)
        return false;

    str_len = strlen(str);
    suffix_len = strlen(suffix);

    if(suffix_len > str_len)
        return false;

    offset = str_len - suffix_len;

    if (strncmp(str + offset, suffix, suffix_len) == 0)
        return true;
    else
        return false;
}

void getlocaltime(struct tm* local_time)
{
    time_t t;

    t = time(NULL);
    local_time = localtime(&t);

    if (local_time == NULL) {
        perror("localtime");
        exit(EXIT_FAILURE);
    }
}

int get_config_path(char* config_path)
{
    char* var;
    struct passwd* pwd;

    var = getenv("XDG_CONFIG_HOME");
    if (var != NULL) {
        snprintf(config_path, PATH_MAX, "%s/filereaper/filereaper.conf", var);
        return 0;
    }

    var = getenv("HOME");
    if (var != NULL) {
        snprintf(config_path, PATH_MAX, "%s/.config/filereaper/filereaper.conf", var);
        return 0;
    }

    pwd = getpwuid(getuid());
    if (pwd != NULL && pwd->pw_dir != NULL) {
        snprintf(config_path, PATH_MAX, "%s/.config/filereaper/filereaper.conf", pwd->pw_dir);
        return 0;
    }

    return -1;
}

char* dictkey(const char* section, const char* value)
{
    /* Allocate memory for the terminating null byte *and* for the colon! */
    char* res = malloc(strlen(section) + strlen(value) + 2);

    strcpy(res, section);
    res = strcat(res, ":");
    res = strcat(res, value);
    return res;
}

// TODO: section_conf -> sconf; default_conf -> dconf
void parse_config_section(dictionary* conf, int section_id, struct config* sect_conf)
{
    const char* section, *path;
    struct config default_conf;
    char *key;
    int v;
    time_t age;
    int flags, keep_empty, min_size, max_size;

    default_conf.flags = 0;

    default_conf.age = iniparser_getint(conf, "default:age", 30);
    default_conf.min_size = iniparser_getint(conf, "default:min_size", 0);
    default_conf.max_size = iniparser_getint(conf, "default:max_size", 0);
    v = iniparser_getboolean(conf, "default:recursive", 0);
    switch (v) {
    case 0:
        default_conf.flags &= ~R_RECURSIVE;
        break;
    case 1:
        default_conf.flags |= R_RECURSIVE;
        break;
    default:
        default_conf.flags &= ~R_RECURSIVE;
        break;
    }

    v = iniparser_getboolean(conf, "default:keep_empty", 0);
    switch (v) {
    case 0:
        default_conf.flags &= ~R_KEEPEMPTY;
        break;
    case 1:
        default_conf.flags |= R_KEEPEMPTY;
        break;
    default:
        default_conf.flags &= ~R_KEEPEMPTY;
        break;
    }

    sect_conf->section = iniparser_getsecname(conf, section_id);
    sect_conf->flags = R_DEFAULT;

    key = dictkey(sect_conf->section, "path");
    sect_conf->path = iniparser_getstring(conf, key, "");
    free(key);

    key = dictkey(sect_conf->section, "age");
    sect_conf->age = (time_t)iniparser_getint(conf, key, default_conf.age);
    free(key);

    key = dictkey(sect_conf->section, "recursive");
    v = iniparser_getboolean(conf, key, -1);
    switch (v) {
    case -1:
        sect_conf->flags |= default_conf.flags & R_RECURSIVE;
        break;
    case 0:
        sect_conf->flags &= ~R_RECURSIVE;
        break;
    case 1:
        sect_conf->flags |= R_RECURSIVE;
        break;
    default:
        sect_conf->flags &= ~R_RECURSIVE;
    }
    free(key);

    key = dictkey(sect_conf->section, "keep_empty");
    v = iniparser_getboolean(conf, key, -1);
    switch (v) {
    case -1:
        sect_conf->flags |= default_conf.flags & R_KEEPEMPTY;
        break;
    case 0:
        sect_conf->flags &= ~R_KEEPEMPTY;
        break;
    case 1:
        sect_conf->flags |= R_KEEPEMPTY;
        break;
    default:
        sect_conf->flags |= default_conf.flags & R_KEEPEMPTY;
    }
    free(key);
}
