/* Copyright (c) 2015 Stefan Tatschner
 *
 * constants.h
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

/* Error Conditions */
enum {
    REAP_OK = 0,
    REAP_BADPATTERN,
    REAP_NAMETOOLONG,
    REAP_BADIO,
};

/* Config Options */
#define R_NONE            0
#define R_VERBOSE         1
#define R_FORCE          (1 << 1)
#define R_RECURSIVE      (1 << 2)
#define R_FOLLOWSYMLINKS (1 << 3)
#define R_DOTFILES       (1 << 4)
#define R_MATCHDIRS      (1 << 5)
#define R_KEEPEMPTY      (1 << 6)
#define R_DEFAULT        R_NONE

/* Limits */
#define MAXDEPTH 25

#endif /* CONSTANTS_H */
